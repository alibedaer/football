<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    //
    protected $table = 'competitions';
    protected $fillable = ['name', 'season_id', 'logo'];

    public function season()
    {
        return $this->belongsTo('LaravelTask\Season', 'season_id');
    }

    public function ranks($id)
    {
        $ranks = Rank::where('competition_id', $id)->orderBy('points', 'DESC')->get();
        return $ranks;
    }
}
