<?php

namespace LaravelTask\Http\Requests;

use LaravelTask\Http\Requests\Request;

class EditMatchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season_id' => 'required',
            'competition_id' => 'required',
            'first_team_id' => 'required',
            'second_team_id' => 'required',
            'result' => 'required'
        ];
    }
}
