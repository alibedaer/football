<?php

namespace LaravelTask\Http\Controllers\Site;

use Illuminate\Http\Request;

use LaravelTask\Club;
use LaravelTask\Competition;
use LaravelTask\Http\Requests;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Player;
use LaravelTask\Season;
use LaravelTask\Team;

class SiteController extends Controller
{

    public function index()
    {
        $teams = Team::paginate(5);
        $competitions = Competition::paginate(5);
        $clubs = Club::paginate(5);
        return view('site.index', compact('teams', 'competitions', 'clubs'));
    }

    public function teams()
    {
        $teams = Team::paginate(6);
        return view('site.teams', compact('teams'));
    }

    public function competitions()
    {
        $competitions = Competition::paginate(6);
        return view('site.competitions', compact('competitions'));
    }

    public function seasons()
    {
        $seasons = Season::paginate(10);
        return view('site.seasons', compact('seasons'));
    }

    public function showSeason($seasonId)
    {
        $season = Season::findOrFail($seasonId);
        return view('site.singleSeason', compact('season'));
    }

    public function showCompetition($competitionId)
    {
        $competition = Competition::findOrFail($competitionId);
        return view('site.singleCompetition', compact('competition'));
    }

    public function clubs()
    {
        $clubs = Club::paginate(6);
        return view('site.clubs', compact('clubs'));
    }

    public function showTeam($teamId)
    {
        $team = Team::findOrFail($teamId);
        return view('site.singleTeam', compact('team'));
    }

    public function showPlayer($playerId)
    {
        $player = Player::findOrFail($playerId);
        return view('site.singlePlayer', compact('player'));
    }

}
