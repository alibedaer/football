<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;
use LaravelTask\Club;
use Illuminate\Support\Facades\File;
use LaravelTask\Http\Controllers\Controller;
use Session;

class ClubsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clubs = Club::paginate(5);

        return view('panel.clubs.index', compact('clubs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('panel.clubs.new');
    }

    /**
     * Store a new club in the storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $club = new Club();
        $rules = ['name' => 'required|min:5|regex:/^[\pL\s\-]+$/u', 'image' => 'required|image'];
        $this->validate($request, $rules);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(6) . "_" . $file->getClientOriginalName();
            $path = 'assets/site/images/clubs/' . $request->input('name');
            $file->move(public_path($path), $fileName);
            $logo = $path . '/' . $fileName;
            $request->merge(['logo' => $logo]);
        }

        $club->create($request->all());
        Session::flash('new_club', 'Club created successfully');

        return redirect('panel/clubs');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $club = Club::findOrFail($id);

        return view('panel.clubs.edit', compact('club'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $club = Club::findOrFail($id);
        $rules = ['name' => 'required|min:5|regex:/^[\pL\s\-]+$/u', 'image' => 'image'];
        $this->validate($request, $rules);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(6) . "_" . $file->getClientOriginalName();
            $path = 'assets/site/images/clubs/' . $request->input('name');
            $file->move(public_path($path), $fileName);
            $logo = $path . '/' . $fileName;
            $request->merge(['logo' => $logo]);
        }

        $club->update($request->all());
        Session::flash('update_club', 'Club updated successfully');

        return redirect('panel/clubs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $club = Club::findOrFail($id);
        File::deleteDirectory(public_path('assets/site/images/clubs/' . $club->name));
        $club->delete();
        Session::flash('delete_club', 'club deleted successfully');

        return redirect('panel/clubs');
    }
}
