<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use LaravelTask\Competition;
use LaravelTask\Http\Requests;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Http\Requests\CreateMatchRequest;
use LaravelTask\Http\Requests\EditMatchRequest;
use LaravelTask\Matche;
use LaravelTask\Rank;
use LaravelTask\Season;
use LaravelTask\Team;
use Session;

class MatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = Matche::paginate(6);
        return view('panel.matches.index', compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seasons = Season::lists('name', 'id');
        $competitions = Competition::lists('name', 'id');
        $competitions[''] = 'select competition';
        $teams = Team::lists('name', 'id');
        $teams[''] = 'select Team';
        return view('panel.matches.new', compact('seasons', 'teams', 'competitions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LaravelTask\Http\Requests\CreateMatchRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMatchRequest $request)
    {

        $match = new Matche();
        $match->create($request->all());
        $teams = ['first_team_id', 'second_team_id'];

        if ($request->input('result') == 0) {
            foreach ($teams as $team) {
                $rank_team = Rank::where('team_id', $request->input($team))->where('season_id', $request->input
                ('season_id'))->where('competition_id', $request->input('competition_id'))->first();

                if ($rank_team === null) {
                    $ranks = new Rank();
                    $point = 1;
                } else {
                    $ranks = Rank::findORFail($rank_team->id);
                    $point = $rank_team->points + 1;
                }
                $ranks->season_id = $request->input('season_id');
                $ranks->competition_id = $request->input('competition_id');
                $ranks->team_id = $request->input($team);
                $ranks->points = $point;
                $ranks->save();
            }
        } else {
            $point = Rank::where('team_id', $request->input('result'))->where('season_id', $request->input
            ('season_id'))->where('competition_id', $request->input('competition_id'))->first();

            if ($point === null) {
                $ranks = new Rank();
                $point = 3;

            } else {
                $ranks = Rank::findOrFail($point->id);
                $point = $point->points + 3;
            }

            $ranks->season_id = $request->input('season_id');
            $ranks->competition_id = $request->input('competition_id');
            $ranks->team_id = $request->input('result');
            $ranks->points = $point;
            $ranks->save();
        }


        Session::flash('new_match', 'match created successfully');
        return redirect('panel/matches');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $matchId
     * @return \Illuminate\Http\Response
     */
    public function edit($matchId)
    {
        $seasons = Season::lists('name', 'id');
        $competitions = Competition::lists('name', 'id');
        $competitions[''] = 'select competition';
        $teams = Team::lists('name', 'id');
        $teams[''] = 'select Team';
        $match = Matche::findOrFail($matchId);
        return view('panel.matches.edit', compact('match', 'seasons', 'teams', 'competitions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LaravelTask\Http\Requests\EditMatchRequest $request
     * @param  int $matchId
     * @return \Illuminate\Http\Response
     */
    public function update(EditMatchRequest $request, $matchId)
    {
        $match = Matche::findOrFail($matchId);
        $match->update($request->all());
        Session::flash('update_match', 'match updated successfully');
        return redirect('panel/matches');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $matchId
     * @return \Illuminate\Http\Response
     */
    public function delete($matchId)
    {
        $match = Matche::findOrFail($matchId);
        $match->delete();
        Session::flash('delete_match', 'Match Deleted Successfully');
        return redirect('panel/matches');
    }

    public function getSeasonCompetitions($seasonId)
    {
        $competitions = Competition::where('season_id', $seasonId)->get();
        return response()->json(['competitions' => $competitions, 'success' => true]);
    }
}
