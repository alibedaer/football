<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Team;
use LaravelTask\TeamGallery;
use Response;
use Session;

class TeamsController extends Controller
{
    //
    public function index()
    {
        $teams = Team::paginate(6);

        return view('panel.teams.index', compact('teams'));
    }

    public function create()
    {
        return view('panel.teams.new');
    }

    public function store(Request $request)
    {
        //        Validation rules
        $rules = [
            'name' => 'required|min:5',
            'logo' => 'required|image',
            'home_kit' => 'required|image',
            'gallery' => 'required',
        ];

//        Gallery images rules

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $k => $image) {
                $rules['gallery.' . $k] = 'image';
            }
        }


//        Validation here

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

//        store in team model [logo,home_kit,away_kit]
        $team = new Team();
        if (!empty($request->file())) {
            $files = ['logo', 'home_kit', 'away_kit'];
            foreach ($files as $file) {
                if ($request->hasFile($file)) {
                    if (!is_array($request->file($file))) {
                        $path = 'assets/site/images/teams/' . $request->input('name');
                        $singleFile = $request->file($file);
                        $fileName = str_random(6) . '_' . $singleFile->getClientOriginalName();
                        $singleFile->move($path, $fileName);
                        $savedName = $path . '/' . $fileName;
                        $team->$file = $savedName;
                    }
                }
            }
        }
        $team->name = $request->input('name');
        $team->save();

//        Storing gallery in gallery model
        if (is_array($request->file('gallery'))) {
            foreach ($request->file('gallery') as $img) {
                $gallery = new TeamGallery();
                $img_name = str_random(6) . '_' . $img->getClientOriginalName();
                $path = 'assets/site/images/teams/' . $request->input('name') . '/gallery';
                $img->move($path, $img_name);
                $photo = $path . '/' . $img_name;
                $gallery->photo = $photo;
                $gallery->team_id = $team->id;
                $gallery->save();
            }
        }

        Session::flash('new_team', 'Team and gallery added successfully');

        return redirect('panel/teams');
    }

//    edit team
    public function edit($teamId)
    {
        $team = Team::findOrFail($teamId);

        return view('panel.teams.edit', compact('team'));
    }

//    update team
    public function update(Request $request, $teamId)
    {

        //        Validation rules
        $rules = [
            'name' => 'required|min:5',
            'logo' => 'image',
            'home_kit' => 'image',
            'gallery' => 'required',
        ];

//        Gallery images rules

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $k => $image) {
                $rules['gallery.' . $k] = 'image';
            }
        }
        
//        Validation here

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

//        store in team model [logo,home_kit,away_kit]
        $team = Team::findOrFail($teamId);
        if (!empty($request->file())) {
            $files = ['logo', 'home_kit', 'away_kit'];
            foreach ($files as $file) {
                if ($request->hasFile($file)) {
                    if (!is_array($request->file($file))) {
                        $path = 'assets/site/images/teams/' . $request->input('name');
                        $singleFile = $request->file($file);
                        $fileName = str_random(6) . '_' . $singleFile->getClientOriginalName();
                        $singleFile->move($path, $fileName);
                        $savedName = $path . '/' . $fileName;
                        $team->$file = $savedName;
                    }
                }
            }
        }
        $team->name = $request->input('name');
        $team->save();

//        updating gallery in gallery model
        if (is_array($request->file('gallery'))) {
            if (count($request->file('gallery')) == 1) {
                foreach ($request->file('gallery') as $img) {
                    if (!empty($img)) {
                        $gallery = new TeamGallery();
                        $img_name = str_random(6) . '_' . $img->getClientOriginalName();
                        $path = 'assets/site/images/teams/' . $request->input('name') . '/gallery';
                        $img->move($path, $img_name);
                        $photo = $path . '/' . $img_name;
                        $gallery->photo = $photo;
                        $gallery->team_id = $teamId;
                        $gallery->save();
                    }
                }
            }
        }

        Session::flash('update_team', 'Team updated successfully');

        return redirect('panel/teams');
    }

//    delete Team
    public function delete($teamId)
    {
        $team = Team::findOrFail($teamId);
        File::delete(public_path('assets/site/images/teams/' . $team->name));
        $team->delete();
        Session::flash('delete_team', 'team deleted successfully');

        return redirect('panel/teams');
    }

//    Delete Gallery Image
    public function deleteGalleryImage($imageId)
    {
        $image = TeamGallery::findOrFail($imageId);
        File::delete(public_path($image->photo));
        $image->delete();

        return response()->json(['success' => true, 'message' => 'image deleted successfully', 'id' => $imageId]);
    }
}
