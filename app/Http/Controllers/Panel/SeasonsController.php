<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;

use LaravelTask\Http\Requests;
use Illuminate\Support\Facades\Validator;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Season;
use Session;

class SeasonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seasons = Season::paginate(6);
        return view('panel.seasons.index', compact('seasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('panel.seasons.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $season = new Season();
        $rules = ['name' => 'required|min:5'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $season->create($request->all());
        Session::flash('new_season', 'season created successfully');
        return redirect('panel/seasons');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $seasonId
     * @return \Illuminate\Http\Response
     */
    public function edit($seasonId)
    {
        $season = Season::findOrFail($seasonId);
        return view('panel/seasons/edit', compact('season'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $seasonId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $seasonId)
    {
        $season = Season::findOrFail($seasonId);
        $rules = ['name' => 'required|min:5'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $season->update($request->all());
        Session::flash('update_season', 'season updated successfully');
        return redirect('panel/seasons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $seasonId
     * @return \Illuminate\Http\Response
     */
    public function delete($seasonId)
    {
        $season = Season::findOrFail($seasonId);
        $season->delete();
        Session::flash('delete_season', 'season deleted successfully');
        return redirect('panel/seasons');

    }
}
