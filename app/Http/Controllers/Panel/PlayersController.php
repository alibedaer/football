<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Player;
use LaravelTask\PlayerGallery;
use LaravelTask\Team;
use Session;

class PlayersController extends Controller
{
    //

    //
    public function index()
    {
        $players = Player::paginate(6);

        return view('panel.players.index', compact('players'));
    }

    public function create()
    {
        $teams = Team::lists('name', 'id');

        return view('panel.players.new', compact('teams'));
    }

    public function store(Request $request)
    {

//        Validation rules
        $rules = [
            'name' => 'required|min:5',
            'birth_date' => 'date',
            'favorite_foot' => 'required',
            'profile' => 'required|image',
            'gallery' => 'required',
        ];

//        Gallery images rules

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $k => $image) {
                $rules['gallery.' . $k] = 'image';
            }
        }


//        Validation here

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

//        store in player model [logo,home_kit,away_kit]
        $player = new Player();
        if ($request->hasFile('profile')) {
            $file = $request->file('profile');

            $fileName = str_random(6) . '_' . $file->getClientOriginalName();
            $path = 'assets/site/images/players/' . $request->input('name');
            $file->move($path, $fileName);
            $profile = $path . '/' . $fileName;
            $player->photo = $profile;
        }
        $player->name = $request->input('name');
        $player->club_id = str_random(5);
        $age = date('Y-m-d') - $request->input('birth_date');
        $player->age = $age;
        $player->birth_date = $request->input('birth_date');
        $player->favorite_foot = $request->input('favorite_foot');
        $player->team_id = $request->input('team_id');
        $player->save();

//        Storing gallery in gallery model
        if (is_array($request->file('gallery'))) {
            foreach ($request->file('gallery') as $img) {
                $gallery = new PlayerGallery();
                $img_name = str_random(6) . '_' . $img->getClientOriginalName();
                $path = 'assets/site/images/players/' . $request->input('name') . '/gallery';
                $img->move($path, $img_name);
                $photo = $path . '/' . $img_name;
                $gallery->photo = $photo;
                $gallery->player_id = $player->id;
                $gallery->save();
            }
        }

        Session::flash('new_player', 'Player and gallery added successfully');

        return redirect('panel/players');
    }

//    edit player
    public function edit($playerId)
    {
        $teams = Team::lists('name', 'id');
        $player = Player::findOrFail($playerId);

        return view('panel.players.edit', compact('player', 'teams'));
    }

//    update player
    public function update(Request $request, $playerId)
    {
        $player = Player::findORFail($playerId);
//        Validation rules
        $rules = [
            'name' => 'required|min:5',
            'birth_date' => 'date',
            'favorite_foot' => 'required',
            'profile' => 'image',
            'gallery' => 'required',
        ];

//        Gallery images rules

        if ($request->hasFile('gallery')) {
            foreach ($request->file('gallery') as $k => $image) {
                $rules['gallery.' . $k] = 'image';
            }
        }


//        Validation here

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

//        store in player model [logo,home_kit,away_kit]
        if ($request->hasFile('profile')) {
            $file = $request->file('profile');

            $fileName = str_random(6) . '_' . $file->getClientOriginalName();
            $path = 'assets/site/images/players/' . $request->input('name');
            $file->move($path, $fileName);
            $profile = $path . '/' . $fileName;
            $player->photo = $profile;
        }
        $player->name = $request->input('name');
        $player->club_id = str_random(5);
        $age = date('Y-m-d') - $request->input('birth_date');
        $player->age = $age;
        $player->birth_date = $request->input('birth_date');
        $player->favorite_foot = $request->input('favorite_foot');
        $player->team_id = $request->input('team_id');
        $player->save();

        if (is_array($request->file('gallery')) && count($request->file('gallery')) == 1) {
            foreach ($request->file('gallery') as $img) {
                if (!empty($img)) {
                    $gallery = new PlayerGallery();
                    $img_name = str_random(6) . '_' . $img->getClientOriginalName();
                    $path = 'assets/site/images/players/' . $request->input('name') . '/gallery';
                    $img->move($path, $img_name);
                    $photo = $path . '/' . $img_name;
                    $gallery->photo = $photo;
                    $gallery->player_id = $player->id;
                    $gallery->save();
                }
            }
        }

        Session::flash('update_player', 'Player and gallery updated successfully');

        return redirect('panel/players');
    }

//    delete Player
    public function delete($playerId)
    {
        $player = Player::findOrFail($playerId);
        File::deleteDirectory(public_path('assets/site/images/players/' . $player->name));
        $player->delete();
        Session::flash('delete_player', 'player deleted successfully');

        return redirect('panel/players');
    }

//    Delete Gallery Image
    public function deleteGalleryImage($imageId)
    {
        $image = PlayerGallery::findOrFail($imageId);
        File::delete(public_path($image->photo));
        $image->delete();

        return response()->json(['success' => true, 'message' => 'image deleted successfully', 'id' => $imageId]);
    }
}
