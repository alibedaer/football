<?php

namespace LaravelTask\Http\Controllers\Panel;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use LaravelTask\Competition;
use LaravelTask\Http\Requests;
use LaravelTask\Http\Controllers\Controller;
use LaravelTask\Season;

class CompetitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $competitions = Competition::paginate(6);
        return view('panel.competitions.index', compact('competitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seasons = Season::lists('name', 'id');
        return view('panel.competitions.new', compact('seasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $competition = new Competition();
        $rules = ['name' => 'required|min:5', 'season_id' => 'numeric', 'image' => 'required|image'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(6) . "_" . $file->getClientOriginalName();
            $path = 'assets/site/images/competitions';
            $file->move(public_path($path), $fileName);
            $logo = $path . '/' . $fileName;
            $request->merge(['logo' => $logo]);
        }

        $competition->create($request->all());
        Session::flash('new_competition', 'competition created successfully');
        return redirect('panel/competitions');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $competitionId
     * @return \Illuminate\Http\Response
     */
    public function edit($competitionId)
    {
        $seasons = Season::lists('name', 'id');
        $competition = Competition::findOrFail($competitionId);
        return view('panel.competitions.edit', compact('competition', 'seasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $competitionId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $competitionId)
    {
        $competition = Competition::findORFail($competitionId);
        $rules = ['name' => 'required|min:5', 'season_id' => 'numeric', 'image' => 'image'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(6) . "_" . $file->getClientOriginalName();
            $path = 'assets/site/images/competitions';
            $file->move(public_path($path), $fileName);
            $logo = $path . '/' . $fileName;
            $request->merge(['logo' => $logo]);
        }
        $competition->update($request->all());
        Session::flash('update_competition', 'competition updated successfully');
        return redirect('panel/competitions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $competitionId
     * @return \Illuminate\Http\Response
     */
    public function delete($competitionId)
    {
        $competition = Competition::findOrfail($competitionId);
        File::delete($competition->logo);
        $competition->delete();
        Session::flash('delete_competition', 'competition deleted successfully');
        return redirect('panel/competitions');
    }
}
