<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//----------------------- Panel Routes Here

Route::group(['middleware' => ['auth']], function () {

//    Panel index Route

    Route::get('/panel', function () {
        return view('panel.index');
    });

//    Clubs Routes

    Route::resource('panel/clubs', 'Panel\ClubsController');

//    Teams Routes

    Route::get('panel/teams', 'Panel\TeamsController@index');
    Route::get('panel/teams/create', 'Panel\TeamsController@create');
    Route::post('panel/teams/create', 'Panel\TeamsController@store');
    Route::get('panel/teams/edit/{teamId}', 'Panel\TeamsController@edit');
    Route::post('panel/teams/edit/{teamId}', 'Panel\TeamsController@update');
    Route::post('panel/teams/delete/{teamId}', 'Panel\TeamsController@delete');
    Route::post('panel/teams/gallery/{imageId}', 'Panel\TeamsController@deleteGalleryImage');

//    Players Routes

    Route::get('panel/players', 'Panel\PlayersController@index');
    Route::get('panel/players/create', 'Panel\PlayersController@create');
    Route::post('panel/players/create', 'Panel\PlayersController@store');
    Route::get('panel/players/edit/{playerId}', 'Panel\PlayersController@edit');
    Route::post('panel/players/edit/{playerId}', 'Panel\PlayersController@update');
    Route::post('panel/players/delete/{playerId}', 'Panel\PlayersController@delete');
    Route::post('panel/players/gallery/{playerId}', 'Panel\PlayersController@deleteGalleryImage');

//    Seasons Routes

    Route::get('panel/seasons', 'Panel\SeasonsController@index');
    Route::get('panel/seasons/create', 'Panel\SeasonsController@create');
    Route::post('panel/seasons/create', 'Panel\SeasonsController@store');
    Route::get('panel/seasons/edit/{seasonId}', 'Panel\SeasonsController@edit');
    Route::post('panel/seasons/edit/{seasonId}', 'Panel\SeasonsController@update');
    Route::post('panel/seasons/delete/{seasonId}', 'Panel\SeasonsController@delete');

//    Competitions Routes

    Route::get('panel/competitions', 'Panel\CompetitionsController@index');
    Route::get('panel/competitions/create', 'Panel\CompetitionsController@create');
    Route::post('panel/competitions/create', 'Panel\CompetitionsController@store');
    Route::get('panel/competitions/edit/{competitionId}', 'Panel\CompetitionsController@edit');
    Route::post('panel/competitions/edit/{competitionId}', 'Panel\CompetitionsController@update');
    Route::post('panel/competitions/delete/{competitionId}', 'Panel\CompetitionsController@delete');

//    Matches Routes

    Route::get('panel/matches', 'Panel\MatchesController@index');
    Route::get('panel/matches/create', 'Panel\MatchesController@create');
    Route::post('panel/matches/create', 'Panel\MatchesController@store');
    Route::get('panel/matches/edit/{matchId}', 'Panel\MatchesController@edit');
    Route::post('panel/matches/edit/{matchId}', 'Panel\MatchesController@update');
    Route::post('panel/matches/delete/{matchId}', 'Panel\MatchesController@delete');

    Route::get('panel/matches/seasonCompetitions/{seasonId}', 'Panel\MatchesController@getSeasonCompetitions');

});


// ----------------------------- Site Routes

Route::get('/', 'Site\SiteController@index');
Route::get('/teams', 'Site\SiteController@teams');
Route::get('/competitions', 'Site\SiteController@competitions');
Route::get('/clubs', 'Site\SiteController@clubs');
Route::get('/teams/{teamId}', 'Site\SiteController@showTeam');
Route::get('/team/players/{playerId}', 'Site\SiteController@showPlayer');
Route::get('/seasons/', 'Site\SiteController@seasons');
Route::get('/seasons/{seasonId}', 'Site\SiteController@showSeason');
Route::get('/competitions/{competitionId}', 'Site\SiteController@showCompetition');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
