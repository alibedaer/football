<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    //
    protected $table = 'seasons';
    protected $fillable = ['name'];

    public function competitions()
    {
        return $this->hasMany('LaravelTask\Competition', 'Season_id');
    }
}
