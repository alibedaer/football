<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    public function GalleryImages()
    {
        return $this->hasMany('LaravelTask\TeamGallery', 'team_id');
    }

    public function players()
    {
        return $this->hasMany('LaravelTask\Player', 'team_id');
    }

    public function ranks()
    {
        return $this->hasMany('LaravelTask\Rank', 'team_id');
    }

}
