<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    //
    public function team()
    {
        return $this->belongsTo('LaravelTask\Team', 'team_id');
    }
}
