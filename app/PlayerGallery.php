<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class PlayerGallery extends Model
{
    //
    protected $table = 'players_galleries';
}
