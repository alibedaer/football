<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    //
    public function GalleryImages()
    {
        return $this->hasMany('LaravelTask\PlayerGallery', 'player_id');
    }

    public function team()
    {
        return $this->belongsTo('LaravelTask\Team', 'team_id');
    }

    public function getFavoriteFootAttribute($value)
    {
        if ($value == 'R') {
            return 'Right';
        } else {
            return 'Left';
        }
    }
}
