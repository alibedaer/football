<?php

namespace LaravelTask;

use Illuminate\Database\Eloquent\Model;


class Matche extends Model
{
    //
    protected $table = 'matches';
    protected $fillable = ['season_id', 'competition_id', 'first_team_id', 'second_team_id', 'result'];

    public function firstTeam()
    {
        return $this->belongsTo('LaravelTask\Team', 'first_team_id');
    }

    public function season()
    {
        return $this->belongsTo('LaravelTask\Season', 'season_id');
    }

    public function competition()
    {
        return $this->belongsTo('LaravelTask\Competition', 'competition_id');
    }

    public function secondTeam()
    {
        return $this->belongsTo('LaravelTask\Team', 'second_team_id');
    }

    /*public function getResultAttribute($value)
    {
        if ($value == 0) {
            return 'Draw';
        } elseif ($value == $this->relations['firstTeam']['id']) {
            return $this->relations['firstTeam']['name'] . " winner";
        } elseif ($value == $this->relations['secondTeam']['id']) {
            return $this->relations['secondTeam']['name'] . " winner";
        } else {
            return 'cancelled';
        }
    }*/

    public function winner($id)
    {
        if ($id == 0) {
            return 'Draw';
        } else {
            return Team::where('id', $id)->first()->name;
        }
    }
}
