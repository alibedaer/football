@extends('auth.layout')
@section('content')
    <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>
        {!! Form::open() !!}
        @if($errors->any())
            <div class="">
                <ul class="list-unstyled">
                    @foreach($errors->all() as $error)
                        <li class="alert alert-danger"> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open() !!}
        <div class="form-group has-feedback">
            <input type="text" class="form-control" name="name" placeholder="Name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="email" class="form-control" name="email" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
        </div>
        {!! Form::close() !!}

        <a href="{{ url('auth/login') }}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
@stop