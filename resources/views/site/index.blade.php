@extends('site.layout')
@section('content')
    <h3> Teams </h3>

    <table class="table table-hover">
        <thead>
        <tr>
            <th> #</th>
            <th> team</th>
            <th> Logo</th>
            <th> players</th>
        </tr>
        <tbody>
        @foreach($teams as $team)
            <tr>
                <td> {{ $team->id }} </td>
                <td><a href="{{ url('/teams/'.$team->id) }}"> {{ $team->name }} </a></td>
                <td>
                    <img src="{{ asset($team->logo) }}" alt="">
                </td>
                <td> {{ $team->players->count() }} </td>
            </tr>
        @endforeach
        </tbody>
        </thead>
    </table>
    <div class="pagination"> {!! $teams->render() !!} </div>

    <hr>

    <h3> Competitions </h3>

    <table class="table table-hover">
        <thead>
        <tr>
            <th> #</th>
            <th> competition</th>
            <th> season</th>

        </tr>
        <tbody>
        @foreach($competitions as $competition)
            <tr>
                <td> {{ $competition->id }} </td>
                <td><a href="{{ url('/competitions/'.$competition->id) }}"> {{ $competition->name }} </a></td>
                <td>
                    <p> {{ $competition->season->name }} </p>
                </td>
            </tr>
        @endforeach
        </tbody>
        </thead>
    </table>
    <div class="pagination"> {!! $teams->render() !!} </div>

    <hr>

    <h3> Club </h3>

    <table class="table table-hover">
        <thead>
        <tr>
            <th> #</th>
            <th> Club</th>
            <th> logo</th>

        </tr>
        <tbody>
        @foreach($clubs as $club)
            <tr>
                <td> {{ $club->id }} </td>
                <td><a href="{{ url('/clubs/'.$club->id) }}"> {{ $club->name }} </a></td>
                <td>
                    <img src="{{ asset($club->logo) }}" alt="">
                </td>
            </tr>
        @endforeach
        </tbody>
        </thead>
    </table>
    <div class="pagination"> {!! $teams->render() !!} </div>
@stop
