@extends('site.layout')
@section('content')
    <style>
        .player {
            margin-top: 20px;
        }

        h2 {
            text-align: center;
        }

        .profile {
            margin-bottom: 15px;
        }

        .profile img {
            width: 100%;
            height: 350px;
        }

        ul li img {
            width: 100%;
            height: 150px;
            margin-bottom: 15px;
        }
    </style>
    <div class="player">
        <div class="row">
            <div class="col-xs-6">
                <div class="profile">
                    <h2> Player Name: {{ $player->name }} </h2>
                    <img src="{{ asset($player->photo) }}" alt="">
                </div>
                <div class="others">
                    <ul class="row list-unstyled">
                        <li><span> Birth Date : </span><span class="pull-right"> {{ date('d/m/Y',strtotime
                        ($player->birth_date)) }}
                            </span>
                        </li>
                        <li>
                            <span> Age: </span>
                            <span class="pull-right"> {{ $player->age }} year </span>
                        </li>
                        <li>
                            <span> Team: </span>
                            <span class="pull-right"> {{ $player->team->name }}  </span>
                        </li>

                        <li>
                            <span> Foot: </span>
                            <span class="pull-right"> {{ $player->favorite_foot }}  </span>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-xs-6">
                <h2> gallery </h2>
                <div class="gallery">
                    <div class="small_img">
                        <ul class="row list-unstyled">
                            @foreach($player->GalleryImages as $img)
                                <li class="col-xs-4"><img src="{{ asset($img->photo) }}" alt=""></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop