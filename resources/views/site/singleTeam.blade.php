@extends('site.layout')
@section('content')
    <style>
        .team {
            margin-top: 20px;
        }

        h2 {
            text-align: center;
        }

        .profile {
            margin-bottom: 15px;
        }

        .profile img {
            width: 100%;
        }

        ul li img {
            width: 100%;
            height: 150px;
            margin-bottom: 15px;
        }
    </style>
    <div class="team">
        <div class="row">
            <div class="col-xs-6">
                <div class="profile">
                    <h2> Club Name: {{ $team->name }} </h2>
                    <img src="{{ asset($team->logo) }}" alt="">
                </div>
                <div class="others">
                    <ul class="row list-unstyled">
                        <li class="col-xs-6">
                            <img src="{{ asset($team->home_kit) }}" alt="">
                            <p> Home Kit </p>
                        </li>
                        <li class="col-xs-6">
                            <img src="{{ asset($team->away_kit) }}" alt="">
                            <p> way Kit </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6">
                <h2> gallery </h2>
                <div class="gallery">
                    <div class="small_img">
                        <ul class="row list-unstyled">
                            @foreach($team->GalleryImages as $img)
                                <li class="col-xs-4"><img src="{{ asset($img->photo) }}" alt=""></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h3> players </h3>
        <ul class="row list-unstyled">
            @foreach($team->players as $player)
                <li class="col-xs-3">
                    <img src="{{ asset($player->photo) }}" alt="">
                    <h4 class="text-center"><a href="{{ url('team/players/'.$player->id) }}">{{ $player->name
                    }}</a></h4>
                </li>
            @endforeach
        </ul>
    </div>
@stop