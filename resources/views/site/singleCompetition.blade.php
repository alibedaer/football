@extends('site.layout')
@section('content')
    <div class="main">
        <style>
            /*            * {
                            text-align: center;
                        }

                        thead tr th {
                            text-align: center;
                        }*/
        </style>
        <div class="main col-xs-12 col-md-6">
            <h3> {{ $competition->name }} / {{ $competition->season->name }}</h3>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th> #</th>
                    <th> Team name</th>
                    <th> Points</th>
                </tr>
                <tbody>
                @foreach($competition->ranks($competition->id) as $k => $com)
                    <tr>
                        <td> {{ $k+1 }} </td>
                        <td><a href="{{ url('teams/'.$com->team->id) }}"> {{ $com->team->name}} </a></td>
                        <td> {{ $com->points }} </td>
                    </tr>
                @endforeach

                </tbody>
                </thead>
            </table>

            <hr>
        </div>
    </div>
@stop