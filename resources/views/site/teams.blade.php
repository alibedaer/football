@extends('site.layout')
@section('content')
    <style>
        .main ul {
            padding: 15px;
        }

        .main ul li {
            margin-bottom: 15px;
        }

        .main ul li .team {
            padding: 15px;
            background-color: #b3faff;
            text-align: center;
        }

        .main ul li img {
            width: 100%;
            height: 250px;
        }
    </style>
    <div class="main">
        <h2> teams </h2>
        <ul class="row list-unstyled">
            @foreach($teams as $team)
                <li class="col-xs-3">
                    <div class="team">
                        <img src="{{ $team->logo }}" alt="">
                        <a href="{{ url('teams/'.$team->id) }}">{{ $team->name }}</a>
                    </div>
                </li>
            @endforeach
        </ul>

        <div class="pagination">
            {{ $teams->render() }}
        </div>
    </div>
@stop