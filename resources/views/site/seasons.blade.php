@extends('site.layout')
@section('content')
    <style>
        * {
            text-align: center;
        }

        thead tr th {
            text-align: center;
        }
    </style>
    <div class="main col-xs-12 col-md-6">
        <h3> seasons </h3>

        <table class="table table-hover">
            <thead>
            <tr>
                <th> #</th>
                <th> season</th>
            </tr>
            <tbody>
            @foreach($seasons as $season)
                <tr>
                    <td> {{ $season->id }} </td>
                    <td><a href="{{ url('/seasons/'.$season->id) }}"> {{ $season->name }} </a></td>
                </tr>
            @endforeach
            </tbody>
            </thead>
        </table>
        <div class="pagination"> {!! $seasons->render() !!} </div>

        <hr>
    </div>
@stop