@extends('site.layout')
@section('content')
    <div class="main">
        <style>
            * {
                text-align: center;
            }

            thead tr th {
                text-align: center;
            }
        </style>
        <div class="main col-xs-12 col-md-6">
            <h3> {{ $season->name }} </h3>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th> #</th>
                    <th> competition name</th>
                    <th> logo</th>
                </tr>
                <tbody>
                @foreach($season->competitions as $competition)
                    <tr>
                        <td> {{ $competition->id }} </td>
                        <td><a href="{{ url('competitions/'.$competition->id) }}"> {{ $competition->name }} </a></td>
                        <td><img src="{{ asset($competition->logo) }}" alt=""></td>
                    </tr>
                @endforeach
                </tbody>
                </thead>
            </table>

            <hr>
        </div>
    </div>
@stop