@extends('panel.layout')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css">
    <style>
        .edit {
            float: left;
            margin-right: 15px;
        }

        img {
            width: 100%;
            height: 150px;
        }
    </style>
@stop
@section('content')

    <section class="content-header">
        <h1>
            List all players
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/players') }}">players</a></li>
            <li class="active">All</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">players</h3>
                    </div>
                    @if(Session::has('new_player'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('new_player') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('update_player'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('update_player') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('delete_player'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('delete_player') }}
                            </div>
                        </div>
                    @endif

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>name</th>
                                <th>logo</th>
                                <th style="width: 40px">Config</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            @foreach($players as $k =>  $player)
                                <tr>
                                    <td class="col-md-3">{{ $k+1 }}</td>
                                    <td class="col-md-3">{{ $player->name }}</td>
                                    <td class="col-md-3">
                                        <img src="{{ asset($player->photo) }}">
                                    </td>
                                    <td class="col-md-3">
                                        <a href="{{ url('panel/players/edit/'.$player->id) }}"
                                           class="btn btn-sm btn-primary edit"> <i
                                                    class="fa fa-pencil-square "></i>
                                        </a>
                                        <button type="submit" id="delete" class="btn btn-sm btn-danger"><i class="fa
                                        fa-trash-o"></i></button>
                                        {!! Form::open(['url'=>url('panel/players/delete/'.$player->id),
                                        'id'=>'delete_form'])
                                         !!}
                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            {!! $players->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <script>
        $('button#delete').on('click', function () {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this player!", type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $("#delete_form").submit();
                    });
        })
    </script>
@stop