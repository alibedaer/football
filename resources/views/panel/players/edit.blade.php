@extends('panel.layout')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css">
    <style>
        img {
            width: 150px;
            height: 150px;
            margin-bottom: 15px;
        }

        .gallery {
            margin-bottom: 15px;
            overflow: hidden;
        }

        .gallery li {
            margin-left: 15px;
            margin-bottom: 15px;
        }

        .gallery img {
            float: left;
            width: 100%;
        }

        .gallery ul li button {
            display: block;
            text-align: center;
            background: red;
            color: #fff;
            font-size: 16px;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            New Club
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/clubs') }}">Clubs</a></li>
            <li class="active">New Club</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Club</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($player,['files'=>true]) !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('name','player Name:') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('team_id','Team:') !!}
                            {!! Form::select('team_id',$teams,null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('birth_date','Birth Date:') !!}
                            {!! Form::input('date','birth_date',date('Y-m-d'),['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('favorite_foot','Favorite Foot:') !!}
                            {!! Form::select('favorite_foot', array('L' => 'Left', 'R' => 'Right'), 'R',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('profile','Profile Picture:') !!}
                            {!! Form::file('profile',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gallery[]','Gallery:') !!}
                            {!! Form::file('gallery[]',['class'=>'form-control','multiple'=>true]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="gallery">
                        <h3> Gallery </h3>
                        <ul class="list-unstyled">
                            @if($player->GalleryImages)
                                @foreach($player->GalleryImages as $image)
                                    <li class="col-xs-2" id="{{ $image->id }}">
                                        <img src="{{ asset($image->photo) }}" alt="">
                                        <button type="submit" class="btn btn-danger btn-block" data-id="{{ $image->id}}"
                                                id="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        {!! Form::open(['url'=>url('panel/players/gallery/'.$image->id),
                                        'id'=>'delete_form_'.$image->id,'class'=>'delete_form'])
                                         !!}

                                        {!! Form::close() !!}
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>

    <script>
        var id;
        $('button#delete').on('click', function (e) {
            e.preventDefault();
            id = $(this).data('id');
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this team!", type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $("#delete_form_" + id).submit();
                        $('.sweet-overlay ,  .sweet-alert').hide();
                    });
        });
        $('.delete_form').submit(function (e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var data = $(this).serialize();
            $.ajax({
                url: link,
                type: 'post',
                data: data,
                success: function (response) {
                    $('.return').append('<div class="alert alert-success">' + response.message + '</div>');
                    $('li#' + response.id).remove();
                }

            });
        })
    </script>
@stop