@extends('panel.layout')
@section('content')
    <section class="content-header">
        <h1>
            New Player
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/Players') }}">Players</a></li>
            <li class="active">New Player</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Player</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['files'=>true]) !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('name','Team Name:') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('team_id','Team:') !!}
                            {!! Form::select('team_id',$teams,null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('birth_date','Birth Date:') !!}
                            {!! Form::input('date','birth_date',date('Y-m-d'),['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('favorite_foot','Favorite Foot:') !!}
                            {!! Form::select('favorite_foot', array('L' => 'Left', 'R' => 'Right'), 'R',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('profile','Profile Picture:') !!}
                            {!! Form::file('profile',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gallery[]','Gallery:') !!}
                            {!! Form::file('gallery[]',['class'=>'form-control','multiple'=>true]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop