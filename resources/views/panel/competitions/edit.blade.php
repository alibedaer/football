@extends('panel.layout')
@section('content')
    <style>
        img {
            width: 150px;
            height: 150px;
        }
    </style>
    <section class="content-header">
        <h1>
            Edit Competition
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/competitions') }}">Competitions</a></li>
            <li class="active">Edit Competition</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Competition</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($competition,['files'=>true,'role'=>'form','url'=>url('panel/competitions/edit/'
                    .$competition->id)])
                     !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('name','competition name:') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('season_id','Season') !!}
                            {!! Form::select('season_id',$seasons,null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('image','Logo :') !!}
                            <img src="{{ asset($competition->logo) }}" alt="">
                            {!! Form::file('image',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop