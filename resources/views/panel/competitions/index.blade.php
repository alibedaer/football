@extends('panel.layout')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css">
    <style>
        .edit {
            float: left;
            margin-right: 15px;
        }

        img {
            width: 100%;
            height: 150px;
        }
    </style>
@stop
@section('content')

    <section class="content-header">
        <h1>
            List all Competitions
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/competitions') }}">Competitions</a></li>
            <li class="active">All</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Competitions</h3>
                    </div>
                    @if(Session::has('new_competition'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('new_competition') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('update_competition'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('update_competition') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('delete_competition'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('delete_competition') }}
                            </div>
                        </div>
                    @endif

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>name</th>
                                <th> Season</th>
                                <th style="width: 40px">Config</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            @foreach($competitions as $k => $competition)
                                <tr>
                                    <td class="col-md-3">{{ $k+1 }}</td>
                                    <td class="col-md-3">{{ $competition->name }}</td>
                                    <td class="col-md-3">{{ $competition->season->name }}</td>
                                    <td class="col-md-3">
                                        <a href="{{ url('panel/competitions/edit/'.$competition->id) }}"
                                           class="btn btn-sm btn-primary edit"> <i
                                                    class="fa fa-pencil-square "></i>
                                        </a>
                                        <button type="submit" id="delete" data-id="{{ $competition->id }}" class="btn
                                        btn-sm
                                        btn-danger"><i
                                                    class="fa
                                        fa-trash-o"></i></button>
                                        {!! Form::open(['url'=>url('panel/competitions/delete/'
                                        .$competition->id),
                                        'id'=>'delete_form_'.$competition->id,'class'=>'delete_form'])
                                         !!}
                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            {!! $competitions->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <script>
        var id;
        $('button#delete').on('click', function (e) {
            e.preventDefault();
            id = $(this).data('id');
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this competition!", type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $("#delete_form_" + id).submit();
                    });
        })
    </script>
@stop