@extends('panel.layout')
@section('content')
    <section class="content-header">
        <h1>
            New Club
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/clubs') }}">Clubs</a></li>
            <li class="active">New Club</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Club</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['files'=>true]) !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('name','Team Name:') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('logo','Club Logo:') !!}
                            {!! Form::file('logo',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('home_kit','Home Kit:') !!}
                            {!! Form::file('home_kit',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('away_kit','away Kit:') !!}
                            {!! Form::file('away_kit',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gallery[]','Gallery:') !!}
                            {!! Form::file('gallery[]',['class'=>'form-control','multiple'=>true]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop