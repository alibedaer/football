@extends('panel.layout')
@section('content')
    <style>
        img {
            width: 150px;
            height: 150px;
            margin-bottom: 15px;
        }
    </style>
    <section class="content-header">
        <h1>
            New Club
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/clubs') }}">Clubs</a></li>
            <li class="active">New Club</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Club</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($club,['files'=>true,'role'=>'form','url'=>url('panel/clubs/'.$club->id),
                    'method'=>'put']) !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('name','Club Name:') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('image','Club Logo:') !!}
                            <img src="{{ asset($club->logo) }}">
                            {!! Form::file('image') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop