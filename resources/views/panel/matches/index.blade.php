@extends('panel.layout')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css">
    <style>
        .edit {
            float: left;
            margin-right: 15px;
        }

        img {
            width: 100%;
            height: 150px;
        }
    </style>
@stop
@section('content')

    <section class="content-header">
        <h1>
            List all matches
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/matches') }}">matches</a></li>
            <li class="active">All</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">matches</h3>
                    </div>
                    @if(Session::has('new_match'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('new_match') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('update_match'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('update_match') }}
                            </div>
                        </div>
                    @endif

                    @if(Session::has('delete_match'))
                        <div class="alert">
                            <div class="alert-success">
                                {{ session('delete_match') }}
                            </div>
                        </div>
                    @endif

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Season</th>
                                <th>Competition</th>
                                <th> First Team</th>
                                <th> Second Team</th>
                                <th> Result</th>
                                <th style="width: 40px">Config</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            @foreach($matches as $k =>  $match)
                                <tr>
                                    <td>{{ $k+1 }}</td>
                                    <td>{{ $match->season->name }}</td>
                                    <td> {{ $match->competition->name }} </td>
                                    <td> {{ $match->firstTeam->name }} </td>
                                    <td> {{ $match->secondTeam->name }} </td>
                                    <td> {{ $match->winner($match->result) }} </td>
                                    <td class="col-md-3">
                                        <a href="{{ url('panel/matches/edit/'.$match->id) }}"
                                           class="btn btn-sm btn-primary edit"> <i
                                                    class="fa fa-pencil-square "></i>
                                        </a>
                                        <button type="submit" id="delete" class="btn btn-sm btn-danger"><i class="fa
                                        fa-trash-o"></i></button>
                                        {!! Form::open(['url'=>url('panel/matches/delete/'.$match->id),
                                        'id'=>'delete_form'])
                                         !!}
                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            {!! $matches->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <script>
        $('button#delete').on('click', function () {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this match!", type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $("#delete_form").submit();
                    });
        })
    </script>
@stop