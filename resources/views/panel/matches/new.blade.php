@extends('panel.layout')
@section('content')
    <section class="content-header">
        <h1>
            New Match
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/matches') }}">Matches</a></li>
            <li class="active">New Match</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Match</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open() !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('season_id','Season:') !!}
                            {!! Form::select('season_id',$seasons,null,['class'=>'form-control season',
                            'data-link'=>url('panel/matches/seasonCompetitions/')])
                             !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('competition_id','Competition:') !!}
                            {!! Form::select('competition_id', $competitions,'',['class'=>'form-control competitions'])
                             !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('first_team_id','First Team:') !!}
                            {!! Form::select('first_team_id',$teams,'',['class'=>'form-control first_team']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('second_team_id','Second Team:') !!}
                            {!! Form::select('second_team_id',$teams,'',['class'=>'form-control second_team']) !!}
                        </div>

                        <div class="form-group">
                            <label for="draw"> Draw: </label>
                            <input type="radio" id="draw" name="result" value="0" checked>
                            <label for="first_team"> First Team won </label>
                            <input type="radio" name="result" value="" id="first_team">
                            <label for="second_team"> Second Team Won </label>
                            <input type="radio" name="result" value="" id="second_team">
                        </div>
                        <div class="form-group">
                            {!! Form::submit('add',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script>
        $('.season').change(function (e) {
            e.preventDefault();
            var url = $(this).data('link') + '/' + $(this).val();
            $.ajax({
                url: url,
                success: function (response) {
                    if (response.success === true) {
                        $('.competitions').empty();
                        $(response.competitions).each(function (index, val) {
                            $('.competitions').append('<option value="' + val.id + ' "> ' + val.name + ' </option>');
                        })
                    }
                }
            })
        });

        $('.first_team').change(function () {
            var id = $(this).val();
            $('#first_team').val(id);
        });

        $('.second_team').change(function () {
            var id = $(this).val();
            $('#second_team').val(id);
        })

    </script>
@stop