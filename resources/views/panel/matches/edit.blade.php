@extends('panel.layout')
@section('content')
    <section class="content-header">
        <h1>
            Edit Match
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/panel') }}"><i class="fa fa-dashboard"></i> panel</a></li>
            <li><a href="{{ url('panel/matches') }}">Matches</a></li>
            <li class="active">Edit Match</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Match</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::model($match) !!}
                    <div class="box-body">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                        <div class="form-group">
                            {!! Form::label('season_id','Season:') !!}
                            {!! Form::select('season_id',$seasons,null,['class'=>'form-control season',
                            'data-link'=>url('panel/matches/seasonCompetitions/')])
                             !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('competition_id','Competition:') !!}
                            {!! Form::select('competition_id', $competitions,null,['class'=>'form-control competitions'])
                             !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('first_team_id','First Team:') !!}
                            {!! Form::select('first_team_id',$teams,null,['class'=>'form-control first_team']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('second_team_id','Second Team:') !!}
                            {!! Form::select('second_team_id',$teams,null,['class'=>'form-control second_team']) !!}
                        </div>

                        <div class="form-group">
                            <label for="draw"> Draw: </label>
                            {!! Form::radio('result','',null,['id'=>'draw']) !!}
                            <label for="first_team"> First Team won </label>
                            {!! Form::radio('result',$match->first_team_id,null,['id'=>'first_team']) !!}
                            <label for="second_team"> Second Team Won </label>
                            {!! Form::radio('result',$match->second_team_id,null,['id'=>'second_team']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Edit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script>
        $('.result').val({{ $match->result }});
        $('.season').change(function (e) {
            e.preventDefault();
            var url = $(this).data('link') + '/' + $(this).val();
            $.ajax({
                url: url,
                success: function (response) {
                    if (response.success === true) {
                        $('.competitions').empty();
                        $(response.competitions).each(function (index, val) {
                            $('.competitions').append('<option value="' + val.id + ' "> ' + val.name + ' </option>');
                        })
                    }
                }
            })
        });

        $('.first_team').change(function () {
            var id = $(this).val();
            $('#first_team').val(id);
        });

        $('.second_team').change(function () {
            var id = $(this).val();
            $('#second_team').val(id);
        })

    </script>
@stop